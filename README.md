# [nuitka](https://github.com/Nuitka/Nuitka) build containers

You can use the container image `registry.gitlab.com/morph027/nuitka-build-box:tag-codename`, see [container registry](https://gitlab.com/morph027/nuitka-build-box/container_registry/3366318).

## trusty libc 2.19

This build container is based upon very ancient Ubuntu Trusty Thar 14.04 to provide the very ancient glibc 2.19. This allows nuitka to build standalone binaries against this old version and the resulting binary should be portable and safe to run on all newer systems.

The toolchain has been beefed up with some new versions:

* gcc/g++ 9 using [ppa:ubuntu-toolchain-r/test](https://launchpad.net/~ubuntu-toolchain-r/+archive/ubuntu/test)
* openssl 1.0.2 using [ppa:0k53d-karl-f830m/openssl](https://launchpad.net/~0k53d-karl-f830m/+archive/ubuntu/openssl)
* python 3.10 using [pyenv](https://github.com/pyenv/pyenv/)
* clang 8 using [llvm](https://apt.llvm.org/trusty/)


## bionic libc 2.27

This build container is based upon ancient Ubuntu Bionic Beaver 18.04 to provide the ancient glibc 2.27. This allows nuitka to build standalone binaries against this old version and the resulting binary should be portable and safe to run on all newer systems.

The toolchain has been beefed up with some new versions:

* gcc/g++ 11 using [ppa:ubuntu-toolchain-r/test](https://launchpad.net/~ubuntu-toolchain-r/+archive/ubuntu/test)
* python 3.12 using [pyenv](https://github.com/pyenv/pyenv/)
* clang 16 using [llvm](https://apt.llvm.org/bionic/)
